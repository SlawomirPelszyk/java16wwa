package pl.sdacademy.apacheCommons;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;

import java.util.HashMap;
import java.util.Map;

public class ApacheTestConsole {

    public static void main(String[] args) {


        String text = "Zagrożenie klimatyczne już okrzyknięto największym " +
                "\ncywilizacyjnym wyzwaniem ${wiek} w. Liderzy bankowego " +
                "\ni ubezpieczeniowego biznesu też coraz częściej rozumieją, " +
                "\nże brudna energetyka wiąże się z potężnym ryzykiem i nie chcą " +
                "\nmieć z nią nic wspólnego. " +
                "\nW UE staje się to normą, w ${kraj} to ciągle nowość."
                + " \nAutor: ${autor}";

        Map<String, String> mapDefinition = new HashMap<String, String>();
        mapDefinition.put("kraj", "Polska");
        mapDefinition.put("wiek", "XXI");
        mapDefinition.put("autor", "Sławek");

        StringSubstitutor sub = new StringSubstitutor(mapDefinition);

        String poZmianie = sub.replace(text);

        System.out.println(poZmianie);

        System.out.println(StringUtils.defaultString(null, "Coś wypisałem - mimo, że mam null"));
        System.out.println(StringUtils.isEmpty(null));
    }
}
