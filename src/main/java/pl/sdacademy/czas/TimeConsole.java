package pl.sdacademy.czas;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class TimeConsole {
    public static void main(String[] args) {
        Date date = new Date();

        System.out.println(date.after(date));

        ZoneId zoneIdPL = ZoneId.of("Europe/Warsaw");
        ZoneId zoneIdNY = ZoneId.of("America/New_York");

        LocalDateTime czasPolska = LocalDateTime.now(zoneIdPL);
        LocalDateTime czasUSA = LocalDateTime.now(zoneIdNY);

        System.out.println(czasPolska);
        System.out.println(czasUSA);

        System.out.println("Czy czas PL jest przed: " + czasPolska.isAfter(czasUSA));
        System.out.println("Czy czas PL jest po: " + czasPolska.isBefore(czasUSA));

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println("W Polsce obecnie jest: " + czasPolska.format(dateTimeFormatter));
        System.out.println("W USA obecnie jest: " + czasUSA.format(dateTimeFormatter));

        LocalDate dataUstawianaRecznie = LocalDate.of(2019, 1, 1);
        System.out.println(dataUstawianaRecznie);

        System.out.println(czasPolska.plusYears(5).format(dateTimeFormatter));
        System.out.println(czasPolska.getHour());
        System.out.println(czasPolska.getDayOfMonth());

    }
}
