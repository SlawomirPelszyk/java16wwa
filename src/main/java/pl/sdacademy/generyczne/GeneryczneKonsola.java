package pl.sdacademy.generyczne;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GeneryczneKonsola {

    public static void main(String[] args) {
        Map<String, String> map1 = new HashMap<>();
        Map<Integer, String> map2 = new HashMap<>();
        //Typy proste nie uczestniczą
        //Map<int, String> map3 = new HashMap<>();

        List<String> list = new ArrayList<>();
        //Typy proste nie uczestniczą
        // List<int> list1 = new ArrayList<>();
    }
}
